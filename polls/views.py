from django.http import HttpResponse, HttpResponseRedirect
from django.template import loader
from django.http import Http404

# shortcut
from django.shortcuts import render, get_object_or_404
from django.urls import reverse
from django.views import generic
from django.utils import timezone

from .models import Question, Choice
from django.contrib.auth.models import User


# from django.contrib.auth.decorators import login_required
from django.contrib.auth.mixins import LoginRequiredMixin


# My index view
def index_old(request):
    return_content = """
    <h1>Hello world</h1>
    <a href="https://docs.djangoproject.com/en/3.2/intro/tutorial03/" >Writing your first Django app, part 3</a>
    {list_content}
    """

    latest_question_list = Question.objects.order_by('pub_date')[:5]

    list_in_display = "".join(["<p>" + q.question_text + "</p>" for q in latest_question_list])

    return HttpResponse(return_content.replace("{list_content}", list_in_display))


# new views function -------------
def index_old2(request):
    latest_question_list = Question.objects.order_by('-pub_date')[:5]
    template = loader.get_template('polls/index.html')
    context = {
        'latest_question_list': latest_question_list,
    }
    return HttpResponse(template.render(context, request))


def index(request):
    latest_question_list = Question.objects.order_by('-pub_date')[:5]
    context = {
        'latest_question_list': latest_question_list,
    }
    return render(request, "polls/index.html", context)


def detail(request, question_id):
    try:
        question = Question.objects.get(pk=question_id)
    except Question.DoesNotExist:
        raise Http404("Question does not exist")
    return render(request, 'polls/detail.html', {'question': question})


def results(request, question_id):
    question = get_object_or_404(Question, pk=question_id)
    return render(request, 'polls/results.html', {'question': question})


# Generic Views ----- This is the final code
class IndexView(LoginRequiredMixin,generic.ListView):
# class IndexView(generic.ListView):
    template_name = 'polls/index.html'
    context_object_name = 'latest_question_list'
    login_url = '/accounts/login/?next=/polls'

    def get_queryset(self):
        # return Question.objects.order_by('-pub_date')[:5]
        # return the last five published questions that is smaller or equal to now
        return Question.objects.filter(pub_date__lte=timezone.now()).order_by('-pub_date')[:6]


class DetailView(generic.DetailView):
    model = Question
    template_name = 'polls/detail.html'

    def get_queryset(self):
        """
        Excludes any questions that aren't published yet.
        """
        return Question.objects.filter(pub_date__lte=timezone.now())


class ResultView(generic.DetailView):
    model = Question
    template_name = 'polls/results.html'

    def get_queryset(self):
        """
        Excludes any questions that aren't published yet.
        """
        return Question.objects.filter(pub_date__lte=timezone.now())


def vote(request, question_id):
    question = get_object_or_404(Question, pk=question_id)
    try:
        selected_choice = question.choice_set.get(pk=request.POST['choice'])
    except (KeyError, Choice.DoesNotExist):
        # Redisplay the question voting form.
        return render(request, "polls/detail.html", {
            'question': question,
            'error_message': "You didn't select a choice."
        })
    else:
        selected_choice.votes += 1
        selected_choice.save()
        # Always return an HttpResponseRedirect after successfully dealing
        # with POST data. This prevents data from being posted twice if a
        # user hits the Back button.
        return HttpResponseRedirect(reverse("polls:results", args=(question_id,)))


# def layout2(request):
#     template = loader.get_template('polls/layout.html')
#     return HttpResponse(template.render(request=request))


# def search(request, question_keyword):
#     questions = Question.objects.filter(question_text__in=question_keyword)
#     return render(request, 'polls/layout.html')

# playing with forms
def enter_name(request):
    """
    direct to the 'enter-name' site
    :param request:
    :return:
    """
    return render(request, 'polls/enter-name.html')


def your_name(request):
    try:
        context = {
            'the_name': request.POST['your_name']
        }
    except:
        context = {}

    return render(request, 'polls/your-name.html', context)
