from django.urls import path
from . import views

app_name = 'formtesting'
urlpatterns = [
    # ex: /polls/
    # the name here is to call this path from a django template with the url command
    path('', views.index, name='index'),
    path('get-name', views.get_name, name='get-name'),
    path('your-name', views.your_name, name='your-name'),
]
