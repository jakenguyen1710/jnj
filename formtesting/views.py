from django.shortcuts import render
from django.http import HttpResponseRedirect

from .forms import NameForm


def index(request):
    return render(request, 'formtesting/index.html')


# Create your views here.
def get_name(request):
    if request.method == 'POST':
        form = NameForm(request.POST)

        if form.is_valid():
            return HttpResponseRedirect('/thanks/')

    else:
        form = NameForm()

    return render(request, 'formtesting/name.html', {'form': form})


def your_name(request):
    return render(request, 'formtesting/your_name.html', {'your_name' : request.POST['your_name']})
