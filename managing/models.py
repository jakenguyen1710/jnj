import datetime
from django.db import models
from django.utils import timezone
from django.contrib import admin


# Create your models here.
class Company(models.Model):
    company_code = models.IntegerField()
    company_name = models.CharField(max_length=100)
    pub_date = models.DateTimeField('date published')

    def __str__(self):
        return str(self.company_code) + ', ' + self.company_name

    @admin.display(boolean=True, ordering='pub_date', description='Published recently?')
    def was_published_recently(self):
        now = timezone.now()
        return now - datetime.timedelta(days=1) <= self.pub_date <= now