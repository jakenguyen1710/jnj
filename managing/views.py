from django.shortcuts import render
from django.http import HttpResponseRedirect
from django.urls import reverse
from django.http import HttpResponse
from .models import Company

import datetime

# Create your views here.
def index_render(request):
    return login_and_comp_selected(request, 'managing/index.html')


def about(request):
    return login_and_comp_selected(request, template_name='managing/about.html')


def comp_sel(request):
    if request.user.is_authenticated:
        all_company = Company.objects.all()
        context = {'comp_list': all_company}
        return render(request, 'managing/comp_sel.html', context)
    else:
        return render(request, 'registration/login.html')


def comp_selected(request):
    if request.user.is_authenticated:
        selected_comp = Company.objects.get(company_code=request.POST['comp'])
        request.session['sel_comp'] = selected_comp.company_name
        # return render(request, 'managing/index.html')
        return HttpResponseRedirect(reverse('managing:index'))
    else:
        return render(request, 'registration/login.html')



def calendar(request):
    if request.user.is_authenticated:

        """
        I want to get the whole date and weekday list in a dictionary manner
        [{1: 'Mon'}, {2: 'Tue'}...]
        """
        today = datetime.datetime.now()
        num_days = days_in_month(today.year, today.month)

        this_month = []

        for i in range(1, num_days + 1):
            this_month += [{'day': i , 'weekday': get_weekday(today.weekday(), today.day - i)}]

        context = {'this_month': this_month}
        return render(request, 'managing/calendar.html', context) # ⭐️Need to add this template to templates/managing
    else:
        return render(request, 'registration/login.html')


def login_and_comp_selected(request, template_name):
    if request.user.is_authenticated and 'sel_comp' in request.session:
        return render(request, template_name)
    elif 'sel_comp' in request.session == False:
        all_company = Company.objects.all()
        context = {'comp_list': all_company}
        return render(request, 'managing/comp_sel.html', context)
    else:
        return render(request, 'registration/login.html')


# ----------------
# -1 is a placeholder for indexing purposes.
_DAYS_IN_MONTH = [-1, 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31]
_WEEKDAY = ['MON', 'TUE', 'WED', 'THU', 'FRI', 'SAT', 'SUN']


def is_leap(year):
    return year % 4 == 0 and (year % 100 != 0 or year % 400 == 0)


def days_in_month(year, month):
    assert 1 <= month <= 12, month
    if is_leap(year) and month == 2:
        return 29
    return _DAYS_IN_MONTH[month]


def get_weekday(base_weekday, days_diff):
    return _WEEKDAY[base_weekday + (int(days_diff/ 7) * 7 - days_diff)] # this one works
    #return _WEEKDAY[(days_diff - base_weekday) % 7] # still working on it


def test_mycode():
    today = datetime.datetime.now()
    num_days = days_in_month(today.year, today.month)

    this_month = []

    for i in range(1, num_days + 1):
        this_month += [{'day': i , 'weekday': get_weekday(today.weekday(), today.day - i)}]
        print(this_month)

    #print(this_month)
