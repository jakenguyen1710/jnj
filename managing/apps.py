from django.apps import AppConfig


class managingConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'managing'
