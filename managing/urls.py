from django.urls import path

from . import views

app_name = 'managing' # K (kakkoi-kouritsu-kantan) ERP

urlpatterns = [
    path('', views.index_render, name='index'),
    path('about/', views.about, name='about'),
    path('comp/', views.comp_sel, name='comp_sel'),
    path('compselected/', views.comp_selected, name='comp_selected'),
    path('calendar/', views.calendar,name='calendar'),
]


